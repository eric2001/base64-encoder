# Base64 Encoder
![Base64 Encoder](./screenshot.jpg) 

## Description
This program will convert a text string to base64, and display the output.  Useful if your trying to log into an SMTP server over telnet, and the server requires authentication.

**License:** [GPL v.3](http://www.gnu.org/licenses/gpl-3.0.html)

## Instructions
Type in the regular text into the "Text to Encode" box, press "Encode", and the result will be displayed in the second box.

## History
**Version 1.0.0:**
> - Initial Release
> - Released on 07 September 2007
>
> Download: [Version 1.0.0 Setup](/uploads/e8dfd28d382d8fce9db74da86db72a92/Base64EncoderSetup.zip) | [Version 1.0.0 Source](/uploads/352a9276aefcc85e612c257b3946a537/Base64EncoderSource.zip)
